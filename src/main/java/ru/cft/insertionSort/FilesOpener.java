package ru.cft.insertionSort;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class FilesOpener {

    private Path dirPath;

    public FilesOpener(String directory) throws IOException{
        dirPath = Paths.get(directory);
        if (!dirPath.isAbsolute()){
            dirPath = dirPath.toAbsolutePath();
        }
        if (!Files.exists(dirPath)){
            throw new IOException("Directory " + directory + " does not exist!");
        }
        if (!Files.isDirectory(dirPath)){
            throw new IOException("File " + directory + " is not a directory!");
        }
    }

    public Map<Path, BufferedReader> getInputStreams() throws IOException{
        Map<Path, BufferedReader> bfReaders = new HashMap<>();
        try(DirectoryStream<Path> dirStream = Files.newDirectoryStream(dirPath)){
            for (Path child: dirStream){
                if (Files.isRegularFile(child)){
                    try{
                        BufferedReader bfReader = Files. newBufferedReader(child, StandardCharsets.UTF_8);
                        bfReaders.put(child, bfReader);
                    }
                    catch (IOException e){
                        System.err.println("Failed to open input file: " + child.getFileName().toString());
                    }

                }
            }
        }
        return bfReaders;
    }

}
