package ru.cft.insertionSort;

import ru.cft.insertionSort.argsParcer.Parser;
import ru.cft.insertionSort.factory.ComparatorFactory;
import ru.cft.insertionSort.sorting.SortersThreadPool;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

public class Main {

    private static boolean isExecuted = false;

    public static void main(String[] args){
        addSignalHandler();
        try{
            Parser parser = new Parser();
            parser.parse(args);
            String directory = parser.getDirectory();
            String prefix = parser.getPrefix();
            ComparatorFactory factory = parser.getComparatorFactory();

            FilesOpener filesOpener = new FilesOpener(directory);
            Map<Path, BufferedReader> filesAndReaders =  filesOpener.getInputStreams();

            SortersThreadPool threadPool = new SortersThreadPool(filesAndReaders, prefix, factory);
            threadPool.execute();
        }
        catch (IOException e){
            System.err.println("Critical error: " + e.getMessage());
        }
        isExecuted = true;
    }

    private static void addSignalHandler(){
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                if(!isExecuted){
                    System.err.println("\nProgram was terminated by signal!");
                }
            }
        });
    }
}
