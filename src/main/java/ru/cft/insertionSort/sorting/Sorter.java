package ru.cft.insertionSort.sorting;

import ru.cft.insertionSort.comparator.ItemComparator;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Sorter {

    private List<String> sortedData = new LinkedList<>(); // linked list has insertion time complexity O(1)
    private ItemComparator comparator;

    public List<String> sort(DataReader reader, ItemComparator comparator) throws ReadFailedException{
        this.comparator = comparator;
        String newLine;

        while (true){
            try{
                newLine = reader.readLine();
                if (newLine == null){
                    break;
                }
                insertItem(newLine);
            }
            catch (ReadFailedException e){
                throw e;
            }
            catch (IOException e){
                System.err.println(e.getMessage());
            }
        }

        return sortedData;
    }

    private void insertItem(String newLine){
        ListIterator<String> iterator = sortedData.listIterator();
        boolean isMax = true;
        while(iterator.hasNext()){
            if (comparator.compare(newLine, iterator.next()) >= 0){
                isMax = false;
                break;
            }
        }
        if (!isMax && iterator.hasPrevious()){
            iterator.previous();
        }
        iterator.add(newLine);
    }
}
