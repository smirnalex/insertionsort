package ru.cft.insertionSort.sorting;

import ru.cft.insertionSort.comparator.ItemComparator;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Path;

public class DataReader {

    private final static int MAX_ERRORS = 100;

    private BufferedReader reader;
    private Path inputPath;
    private ItemComparator comparator;
    private int numErrors = 0;

    public DataReader(BufferedReader reader, Path inputPath, ItemComparator comparator){
        this.reader = reader;
        this.inputPath = inputPath;
        this.comparator = comparator;
    }

    public String readLine() throws IOException{
        String newLine;
        try {
            newLine = reader.readLine();
        }
        catch(IOException e){
            numErrors++;
            if (numErrors > MAX_ERRORS){
                throw new ReadFailedException("Failed to read file " + inputPath.getFileName().toString());
            }
            throw new IOException("File " + inputPath.getFileName().toString() + " : Failed to read a line");
        }

        if(newLine != null && !comparator.isCorrect(newLine)){
            throw new IOException("File " + inputPath.getFileName().toString() + " : \"" + newLine + "\" has incorrect type");
        }
        return newLine;
    }

    public void closeStream(){
        try{
            reader.close();
        }
        catch (IOException e){
            System.err.println("Failed to close file " + inputPath.getFileName().toString());
        }
    }
}

class ReadFailedException extends IOException{
    ReadFailedException(String message){
        super(message);
    }
}
