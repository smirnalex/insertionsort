package ru.cft.insertionSort.sorting;

import ru.cft.insertionSort.comparator.ItemComparator;
import ru.cft.insertionSort.factory.ComparatorFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SortersThreadPool {

    private List<Runnable> tasks;

    public SortersThreadPool(Map<Path, BufferedReader> bufferedReaders, String prefix, ComparatorFactory factory){
        tasks = new LinkedList<>();
        for (Map.Entry<Path, BufferedReader> pair: bufferedReaders.entrySet()){

            Runnable task = () -> {
                try{
                    ItemComparator comparator = factory.createComparator();
                    Path inputPath = pair.getKey();
                    BufferedReader bfReader = pair.getValue();
                    String outputFileName= inputPath.getParent().toString() + File.separator +
                            prefix + inputPath.getFileName().toString();

                    DataReader reader = new DataReader(bfReader, inputPath,  comparator);
                    Sorter sorter = new Sorter();
                    SortedDataWriter writer = new SortedDataWriter(outputFileName);

                    List<String> sortedData = sorter.sort(reader, comparator);
                    reader.closeStream();

                    writer.write(sortedData);
                    writer.closeStream();
                }
                catch (IOException e){
                    System.err.println(e.getMessage());
                }
            };

            tasks.add(task);
        }
    }

    public void execute(){
        ExecutorService service = Executors.newCachedThreadPool();
        for (Runnable task: tasks){
            service.submit(task);
        }
        service.shutdown();
    }
}
