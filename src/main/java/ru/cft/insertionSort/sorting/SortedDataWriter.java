package ru.cft.insertionSort.sorting;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class SortedDataWriter {

    private BufferedWriter writer;
    private Path outputPath;

    public SortedDataWriter(String fileName) throws IOException {
        try{
            outputPath = Paths.get(fileName);
            writer  = Files.newBufferedWriter(outputPath, StandardCharsets.UTF_8);
        }
        catch (IOException e){
            throw new IOException("Failed to open output file " + outputPath.getFileName().toString());
        }
    }

    public void write(List<String> sortedData){
        for (String line: sortedData){
            try{
                writer.write(line);
                writer.newLine();
            }
            catch (IOException e){
                System.err.println("Output file " + outputPath.getFileName().toString() + " : Failed to write a line");
            }
;
        }
    }

    public void closeStream(){
        try{
            writer.close();
        }
        catch (IOException e){
            System.err.println("Failed to close file " + outputPath.getFileName().toString());
        }
    }
}
