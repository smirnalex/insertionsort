package ru.cft.insertionSort.factory;

import ru.cft.insertionSort.comparator.IntegerComparator;

public class IntegerComparatorFactory extends ComparatorFactory{
    @Override
    public IntegerComparator createComparator(){
        return new IntegerComparator(order);
    }
}

