package ru.cft.insertionSort.factory;

import ru.cft.insertionSort.comparator.ItemComparator;
import ru.cft.insertionSort.comparator.Order;

public abstract class ComparatorFactory{

    protected Order order;

    public abstract ItemComparator createComparator();

    public void setOrder(Order order){
        this.order = order;
    }
}
