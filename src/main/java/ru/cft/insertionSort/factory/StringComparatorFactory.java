package ru.cft.insertionSort.factory;

import ru.cft.insertionSort.comparator.StringComparator;

public class StringComparatorFactory extends ComparatorFactory{
    @Override
    public StringComparator createComparator(){
        return new StringComparator(order);
    }
}
