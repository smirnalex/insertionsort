package ru.cft.insertionSort.argsParcer;

import com.google.devtools.common.options.Option;
import com.google.devtools.common.options.OptionsBase;

public class SortOptions extends OptionsBase{
    @Option(
            name = "help",
            abbrev = 'h',
            help = "Prints usage info.",
            defaultValue = "true"
    )
    public boolean help;

    @Option(
            name = "out-prefix",
            abbrev = 'p',
            help = "Prefix of the output files.",
            defaultValue = "sorted_",
            category = "startup"
    )
    public String prefix;

    @Option(
            name = "content-type",
            abbrev = 't',
            help = "Type of the sorted data: string (s) or integer (i).",
            defaultValue = "i",
            category = "startup"
    )
    public String type;

    @Option(
            name = "sort-mode",
            abbrev = 'm',
            help = "Sort order: ascending (a) or descending (d).",
            defaultValue = "a",
            category = "startup"
    )
    public String mode;
}
