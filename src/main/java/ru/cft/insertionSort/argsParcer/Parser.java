package ru.cft.insertionSort.argsParcer;

import com.google.devtools.common.options.OptionsParser;
import ru.cft.insertionSort.factory.*;
import ru.cft.insertionSort.comparator.Order;

import java.io.IOException;
import java.util.Collections;

public class Parser {

    private OptionsParser parser;
    private SortOptions options;
    private String directory;

    public void parse(String[] args) throws IOException{

        parser = OptionsParser.newOptionsParser(SortOptions.class);
        parser.parseAndExitUponError(args);
        options = parser.getOptions(SortOptions.class);

        if (args.length < 1) {
            throw new IOException("You did not specify the directory of the input files!\n" + getUsage());
        }
        else{
            directory = args[0];
        }
    }

    public String getDirectory(){
        return directory;
    }

    public String getPrefix() throws IOException{
        if (options.prefix.isEmpty()) {
            throw new IOException("Empty prefix of the output files!\n" + getUsage());
        }
        else{
            return options.prefix;
        }
    }

    public ComparatorFactory getComparatorFactory() throws IOException{
        ComparatorFactory factory;

        if (options.type.equals("i")){
            factory = new IntegerComparatorFactory();
        }
        else if (options.type.equals("s")){
            factory = new StringComparatorFactory();
        }
        else{
            throw new IOException("Wrong type of the sorted data!\n" + getUsage());
        }

        if(options.mode.equals("a")){
            factory.setOrder(Order.ASCENDING);

        }
        else if (options.mode.equals("d")){
            factory.setOrder(Order.DESCENDING);
        }
        else{
            throw new IOException("Wrong sort order!\n" + getUsage());
        }

        return factory;
    }

    private String getUsage(){
        String usage = "Usage: java -jar InsertionSort.jar <directory path> [OPTIONS]\n";
        String options = parser.describeOptions(Collections.emptyMap(), OptionsParser.HelpVerbosity.LONG);
        return usage + options;
    }
}
