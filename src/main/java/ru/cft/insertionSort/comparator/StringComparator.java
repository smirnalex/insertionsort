package ru.cft.insertionSort.comparator;

public class StringComparator extends ItemComparator {

    public StringComparator(Order order){
        super(order);
    }

    @Override
    public int compare(String str1, String str2){
        if(order == Order.ASCENDING){
            return str2.compareTo(str1);
        }
        else{
            return str1.compareTo(str2);
        }
    }

    @Override
    public boolean isCorrect(String str){
        return !str.contains(" ");
    }
}
