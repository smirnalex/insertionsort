package ru.cft.insertionSort.comparator;

public abstract class ItemComparator {
    Order order;

    public ItemComparator(Order order){
        this.order = order;
    }

    public abstract boolean isCorrect(String str);

    public abstract int compare(String a, String b);
}
