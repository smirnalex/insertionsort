Инструкция по запуску:

Скачать jar архив InsertionSort.jar из out/artifacts/InsertionSort
и выполнить команду java -jar InsertionSort.jar <directory path> [OPTIONS]

Для парсинга аргументов командной строки использовалась сторонняя библиотека https://github.com/pcj/google-options , 
основанная на аннотациях

Для ее подключения использовалась система сборки проектов Maven